def vowel_swapper(string):
    # ==============
    # Your code here
    vowel_replacement = {"a": "4", "e": "3", "i": "!", "o": "ooo", "u": "|_|"}
    new_string = ""
    for char in string:
        if char != " " and char != "O":
            new_char = vowel_replacement.get(char.lower())
            if new_char:
                new_string += new_char
            else:
                new_string += char
        elif char == "O":
            new_string += "000"
        elif char == " ":
            new_string += " "

    return new_string
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console