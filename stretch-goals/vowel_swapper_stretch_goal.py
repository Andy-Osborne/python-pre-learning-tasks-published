def vowel_swapper(string):
    # ==============
    # Your code here
    swap_list = []
    char_list = set()
    vowel_replacement = {"a": "/\\", "e": "3", "i": "!", "o": "ooo", "O": "000", "u": "|_|"}

    # Adds each character to a list to check and adds unique chars to a set for later use
    for char in string:
        swap_list.append(char)
        char_list.add(char.lower())

    # Adding unique characters to dictionary from set
    count_dictionary = {}
    for char in char_list:
        count_dictionary[char] = 0

    # Iterates over the length of the swap list
    for i in range(len(swap_list)):

        # For each letter in the list, it changes to lowercase then checks the dict and increments value
        count_dictionary[swap_list[i].lower()] += 1

        # Assigns the current count value to variable from dict
        count = count_dictionary[swap_list[i].lower()]

        current_letter = swap_list[i]

        if current_letter != " ":

            # Checks if the current letter is in the vowel_replacement dictionary
            if current_letter.lower() in vowel_replacement:

                # If the count in the dictionary is 2, it resets to 0 and replaces the letter accordingly.
                if count == 2:
                    count_dictionary[swap_list[i].lower()] = 0
                    if current_letter != "O":
                        swap_list[i] = vowel_replacement.get(swap_list[i].lower())
                    else:
                        swap_list[i] = vowel_replacement.get(swap_list[i])

    new_string = "".join(swap_list)

    return new_string








    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
