def factors(number):
    # ==============
    # Your code here

    factor_list = []
    for i in range(1, number+1):
        if number % i == 0:
            if i != 1 and i != number:
                factor_list.append(i)

    if len(factor_list) == 0:
        factor_list = f"{number} is a prime number"

    return factor_list
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
