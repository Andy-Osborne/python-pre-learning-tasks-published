def calculator(a, b, operator):
    # ==============
    calc = {"+": a + b, "-": a - b, "*": a * b, "/": int(a / b), "%": a % b}
    binary_calc = bin(calc[operator])[2:]
    return binary_calc
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
